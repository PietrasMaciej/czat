/* jshint node: true */
var express = require('express');
var app = express();
var httpServer = require("http").Server(app);
var io = require("socket.io")(httpServer);

// var static = require('serve-static');
var port = process.env.PORT || 8000;

app.use('/lib/', express.static(__dirname + '/bower_components/jquery/dist/'));
app.use(express.static(__dirname + '/public'));


// usernames which are currently connected to the chat
var usernames = [];
var history = {};

// rooms which are currently available in chat
var rooms = ['room1','room2','room3'];

io.sockets.on("connection", function (socket) {

	console.log('a user connected');

    	//console.log('wiadomość: ' + msg);
			// when the client emits 'sendchat', this listens and executes
		 socket.on('sendchat', function(msg){
			 // we tell the client to execute 'updatechat' with 2 parameters
     io.emit('updatechat', socket.username, msg);
			//dodanie wpisu klienta do tablicy
		 history[msg]=msg;
		//aktualizowanie historii wpisow w czacie, po stronie klienta
		 io.sockets.emit('updatehistory', history);
		 });

			// when the client emits 'adduser', this listens and executes
			socket.on('adduser', function(username, callback){
				if (usernames.indexOf(username) != -1) {
					callback(false);
					socket.username = username;
				} else {
					callback(true);
					// we store the username in the socket session for this client
					socket.username = username;
					usernames.push(socket.username);
					// add the client's username to the global list
					//usernames[username] = username;
					// echo to client they've connected
					socket.emit('updatechat', 'SERWER', 'Witaj, połączyłeś się z czatem');
					// echo globally (all clients) that a person has connected
					socket.broadcast.emit('updatechat', 'SERWER', username + ' połączył się');
					console.log(usernames);
				}
			});

		 	// when the user disconnects.. perform this
		 	socket.on('disconnect', function(){
		 		// remove the username from global usernames list
				usernames.splice( usernames.indexOf(socket.username), 1 );
		 		//delete usernames[socket.username];
				console.log(usernames);
		 		// update list of users in chat, client-side
		 		io.sockets.emit('updateusers', usernames);
		 		// echo globally that this client has left
		 		socket.broadcast.emit('updatechat', 'SERWER', socket.username + ' opuścił czat');
		 	});


//    socket.on("message", function (data) {
//        io.sockets.emit("aqq", "No tak, tak – „" + data + "”");
//    });
    socket.on("error", function (err) {
        console.dir(err);
    });
});

httpServer.listen(port, function () {
    console.log('Serwer HTTP działa na porcie ' + port);
});
