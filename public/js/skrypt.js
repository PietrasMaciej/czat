/* jshint browser: true, globalstrict: true, devel: true */
/* global io: false */
/*global io: false, $: false*/
"use strict";

// Inicjalizacja
$(window).load(function (event) {
    var status = $("#status")[0];
    var open = $("#open")[0];
    var close = $("#close")[0];
    var send = $("#send")[0];
    var text = $("#text")[0];
    var message = $("#message")[0];
    var err = $("#err");
    var socket;
	  var username;

    status.textContent = "Brak połącznia";

    $("#close").prop('disabled', true);
    $("#send").prop('disabled', true);

    // Po kliknięciu guzika „Połącz” tworzymy nowe połączenie WS
    $("#open").click(function (event) {
        //$("#open").prop('disabled', true);
        //if (!socket || !socket.connected) {
            socket = io({forceNew: true});
        //}

		//if($("#user").val()) {
        // on connection to server, ask for user's name with an anonymous callback
        socket.on('connect', function () {
            var login = $('#users').val();
            // call the server-side function 'adduser' and send one parameter (login)
            socket.emit('adduser', login, function (data) {
              if (data) {
                close.disabled = false;
                send.disabled = false;
                $("#open").prop('disabled', true);
                status.src = "img/bullet_green.png";
                console.log('Nawiązano połączenie przez Socket.io');
              } else {
                $("#open").prop('disabled', false);
                err.html("Użytkownik o podanym loginie już jest na czacie. Wybierz inny.");
                //socket.emit('adduser', login);
                err.delay(3200).fadeOut(300);
              }
            });
            $('#users').val('');
        });

        socket.on('disconnect', function () {
            open.disabled = false;
            status.src = "img/bullet_red.png";
            console.log('Połączenie przez Socket.io zostało zakończone');
        });
        socket.on("error", function (err) {
            message.textContent = "Błąd połączenia z serwerem: '" + JSON.stringify(err) + "'";
        });
        socket.on("aqq", function (data) {
            message.textContent = "Serwer: " + data;
        });
        // listener, whenever the server emits 'updatechat', this updates the chat body
  		 socket.on('updatechat', function(username, msg){
  			 $('#message').append($('<p>').text(username + ": " + msg));
  		 });

       // listener, whenever the server emits 'updatehistory', this updates the msg list
    	socket.on('updatehistory', function(data) {
    		$('#usersy').empty();
    		$.each(data, function(key, value) {
    			$('#usersy').append('<div>' + key + '</div>');
    		});
	});

    });

    // Zamknij połączenie po kliknięciu guzika „Rozłącz”
    $("#close").click(function (event) {
        $("#close").prop('disabled', true);
        $("#send").prop('disabled', true);
        $("#open").prop('disabled', false);
        message.textContent = "";
        socket.io.disconnect();
        console.dir(socket);
    });

    // Wyślij komunikat do serwera po naciśnięciu guzika „Wyślij”
    $("#send").click(function (event) {
//        socket.emit('Wiadomość', text.value);
//        console.log('Wysłałem wiadomość: ' + text.value);
//        text.value = "";
  // tell server to execute 'sendchat' and send along one parameter
		socket.emit('sendchat', $('#text').val());
			$('#text').val('');
    		return false;
    });//DHC - REST/HTTP API CLIENT

});
